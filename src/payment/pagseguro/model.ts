import { Checkout } from "../model";
import { MongoDbModel } from "../../core/mongodb-model";

export interface PlaceOrderResponse extends Checkout {
    src: "pagSeguro";
}

export interface PagSeguroNotification {
    _id: string;
    type: string;
    date: Date;
    checked: boolean;
}
export class PagSeguroNotificationModel extends MongoDbModel<PagSeguroNotification> {
    constructor() {
        super("pagSeguroNotification");
    }
}