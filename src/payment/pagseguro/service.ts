import * as rp from "request-promise-native";
import { Service } from "../../core/service";
import { ProductRefFinder, ProductRef } from "../model";
import { XML } from "../../core/xml";
import { PlaceOrderResponse, PagSeguroNotificationModel, PagSeguroNotification } from "./model";


// // Prod
// const url = "https://ws.pagseguro.uol.com.br";
// const apiKey = "FD1E6E6BDDDDCA922477FF8BC321877F";

// Sandbox
const url = "https://ws.sandbox.pagseguro.uol.com.br";
const apiKey = "575686CC42426C6004B85F8501951FD0";
const user = "katimil@yahoo.com";


export class PagSeguroService implements Service {
    private static get token() {
        return `token=${apiKey}&email=${user}`;
    }

    static get srcName(): "pagSeguro" {
        return "pagSeguro";
    }

    protected async getCheckoutCode(_id: any, qtd: number, referenceId: string, product: ProductRef) {
        const token = PagSeguroService.token;
        if (product.price === undefined || product.price === null) {
            throw new Error("Unexpected price " + product.price);
        }
        const price = typeof product.price === 'number' ? product.price.toFixed(2) : product.price;
        const form = {
            currency: "BRL",
            itemId1: _id,
            itemDescription1: product.description,
            itemAmount1: price,
            itemQuantity1: qtd,
            reference: referenceId
        };
        return await rp.post(`${url}/v2/checkout?${token}`, {
            form: form
        });
    }

    async saveNotification(code: string, type: string) {
        const model = new PagSeguroNotificationModel();
        const notification: PagSeguroNotification = {
            _id: code,
            type: type,
            date: new Date(),
            checked: false
        };
        return await model.create(notification);
    }

    async getNotificationDetails(code: string) {
        const token = PagSeguroService.token;
        const body = await rp.get(
            `${url}/v3/transactions/notifications/${code}?${token}`
        );
        const xml = await XML.parse(body);

        return xml.transaction;
    }

    async placeOrder(productId: string, qtd?: number, type?: string): Promise<PlaceOrderResponse> {
        const model = new ProductRefFinder();
        let product: ProductRef | null;
        if (!type || type === "menu") {
            product = null//await model.findMenuById(productId);
        } else {
            product = null//await model.findProductById(productId);
        }
        if (!product) {
            throw new Error("Product not found");
        }
        qtd = Math.max(qtd || 0, 1);
        const referenceId = ProductRefFinder.Id().toHexString();
        const body = await this.getCheckoutCode(productId, qtd, referenceId, product)
        const xml = await XML.parse(body);
        return {
            checkoutCode: xml.checkout.code,
            date: new Date(xml.checkout.date),
            src: PagSeguroService.srcName,
            product: {
                id: productId,
                price: 156,
                qtd: qtd
            },
            referenceId: referenceId,
            status: 0
        };
    }
}
