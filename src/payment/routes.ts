import { Routing, RequestParams } from "../core/routing";
import { PagSeguroService } from "./pagseguro/service";
import { PaymentService } from "./service";
import { Params, Id } from "@feathersjs/feathers";
import { NotFound, BadRequest, PaymentError, NotAuthenticated } from "@feathersjs/errors";
import { sendReservaProntaParaPagar } from "../email/reserva-pronta-para-pagar";
import { Base64 } from "../core/base64";
import { ConfigStorage } from "../core/config";


export class PaymentRoutes extends Routing {
    constructor()  {
        super("/payment");
    }

    @Routing.Find("/status")
    public async status() {
        return "ok";
    }

    private createNewZetsegChallenge(id: Id) {
        const paymentService = new PaymentService();
        return paymentService.encryptId(id);
    }



    @Routing.Get("/ordersByNotification")
    public async ordersByNotification(id: Id) {
        const pagSeguroService = new PagSeguroService();
        const code = id.toString();
        const details = await pagSeguroService.getNotificationDetails(code);
        const paymentService = new PaymentService();
        await paymentService.saveCheckout(details.reference, details.status, "pagSeguro");
        return details;
    }

    @Routing.Create("/pagSeguro/ndt/notification")
    public async notification(
        data: {notificationCode: string, notificationType: string}, 
        params?: RequestParams
    ) {
        if (!params) {
            throw 1;
        }
        if (params.headers.host !== "pagseguro.uol.com.br") {
            throw new NotFound();
        } else if (!data || !data.notificationCode || data.notificationType !== "transaction") {
            throw new BadRequest("Invalid Payload", data);
        }

        const pagSeguroService = new PagSeguroService();
        await pagSeguroService.saveNotification(data.notificationCode, data.notificationType);
        await this.ordersByNotification(data.notificationCode);

        return "ok";
    }

    @Routing.Create("/pagSeguro/placeOrder")
    public async placeOrder(data: {productId: string, qtd?: number, type?: string}): Promise<{checkoutCode: string}> {
        if (!data.productId) {
            throw new BadRequest("Invalid payload");
        }
        const pagSeguroService = new PagSeguroService();
        try {
            const checkout = await pagSeguroService.placeOrder(data.productId, data.qtd, data.type);
            const paymentService = new PaymentService();
            await paymentService.createCheckout(checkout);
            return {checkoutCode: checkout.checkoutCode};
        } catch (error) {
            throw error;
        }
    }

    @Routing.Create("/pagSeguro/confirmTransaction")
    public async confirmTransaction(
        data: {transactionCode: string, productId: string, checkoutCode: string},
        params?: RequestParams
    ): Promise<{purchaseId: string}> {
        if (!data.transactionCode || !data.productId || !data.checkoutCode) {
            throw new BadRequest("Invalid payload");
        }
        const paymentService = new PaymentService();
        const purchaseId = await paymentService.confirmTransaction(
            data.transactionCode, 
            data.productId, 
            data.checkoutCode,
            PagSeguroService.srcName
        );
        if (!purchaseId) {
            throw new PaymentError("Invalid purchaseId");
        }
        return {purchaseId: purchaseId};
    }
}
