import * as rp from "request-promise-native";
import { Service } from "../core/service";
import { CheckoutModel, Checkout, TransactionModel } from "./model";
import { Id } from "@feathersjs/feathers";


export class PaymentService implements Service {
    async createCheckout(checkout: Checkout) {
        const model = new CheckoutModel();
        return await model.create(checkout);
    }

    async saveCheckout(referenceId: string, status: number, src: string) {
        const model = new CheckoutModel();
        return await model.updateOne({referenceId: referenceId, src: src}, {status: status});
    }

    async confirmTransaction(transactionCode: string, productId: string, checkoutCode: string, src: string) {
        const model = new TransactionModel();
        const transactions = await model.create({
            transactionCode: transactionCode, 
            productId: productId, 
            checkoutCode: checkoutCode, 
            src: src
        });
        let transaction = transactions[0];
        return transaction._id;
    }

    encryptId(id: Id): string {
        return id.toString()
            .replace(/2/g, '@')
            .replace(/7/g, '%')
            .replace(/0/g, '7')
            .replace(/8/g, '2')
            .split('').reverse().join('');
    }

    decryptId(id: Id): string {
        return id.toString()
            .replace(/2/g, '8')
            .replace(/7/g, '0')
            .replace(/@/g, '2')
            .replace(/%/g, '7')
            .split('').reverse().join('');
    }
}
