import { expect } from 'chai';
import 'mocha';
import { Serverlet } from '../app';
import { PaymentRoutes } from './routes';

const serverlet = new Serverlet().setup();
const app = serverlet.installRoutingProvider(new PaymentRoutes()).app;

describe('Payment service', () => {
    const service = app.service('/payment');
    
    it('should rerturn "ok"', () => {
        const name = "ok";
        service.find().then(result => {
            expect(result).to.be.equals(name);
        }).catch(error => {
            throw error;
        });
    });
});