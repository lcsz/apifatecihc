import { MongoDbModel } from "../core/mongodb-model";
import { Model } from "../core/persisted-model";


export interface ProductRef extends Model {
    price: number;
    description: string;
};
export class ProductRefFinder {
    public static Id = MongoDbModel.Id;


}


export interface Checkout extends Model {
    checkoutCode: string;
    date: Date;
    src: string;
    product: {
        id: string;
        price: number;
        qtd: number;
    };
    referenceId: string;
    status: number;
}
export class CheckoutModel extends MongoDbModel<Checkout> {
    constructor() {
        super("checkout");
    }
}


export interface Transaction extends Model {
    transactionCode: string;
    productId: string;
    checkoutCode: string;
    src: string;
}
export class TransactionModel extends MongoDbModel<Transaction> {
    constructor() {
        super("transaction");
    }
}
