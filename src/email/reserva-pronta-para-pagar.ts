import { sendEmailTemplate } from "../core/email";
import { Booking } from "../products/model";

interface ReservaProntaParaPagar {
    userName: string, 
    paymentUrl: string
}

export async function sendReservaProntaParaPagar(booking: Booking, paymentUrl: string) {
    if (booking._id === undefined || booking._id === null) {
        throw new Error("Invalid _id");
    }
    const templateName = 'reserva-pronta-para-pagar';
    const subject = 'Sua Reserva no O Chef Gastronomia'
    const data: ReservaProntaParaPagar = {
        userName: booking.name,
        paymentUrl: paymentUrl
    };
    return await sendEmailTemplate(booking.email, subject, templateName, data);
}
