import { sendEmailTemplate } from "../core/email";
import { Booking } from "../products/model";

interface ReservaRecebidaParams {
    userName: string, 
    cdReserva: string
}

export async function sendReservaRecebida(booking: Booking) {
    if (booking._id === undefined || booking._id === null) {
        throw new Error("Invalid _id");
    }
    const templateName = 'reserva-recebida';
    const subject = 'Sua Reserva no O Chef Gastronomia'
    const data: ReservaRecebidaParams = {
        userName: booking.name,
        cdReserva: booking._id.toString()
    };
    return await sendEmailTemplate(booking.email, subject, templateName, data);
}
