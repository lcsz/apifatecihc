import { Routing, RequestParams, Method } from "../core/routing";
import { Params, Id } from "@feathersjs/feathers";
import { NotFound, BadRequest, PaymentError } from "@feathersjs/errors";

import {
    TipoProjeto, TipoProjetoModel, Clientes, ClientesModel, IhcPrincipalModel, IhcPrincipal
} from './model';
import { CRUDMethods } from '../core/crud';


import { sendReservaRecebida } from "../email/reserva-recebida";
import { sendContato } from "../email/contato";
import { PushService } from "../core/push";


// @CRUDMethods('/reserva', BookingModel, [Method.Get, Method.Create])
export class ProductsRoutes extends Routing {
    constructor() {
        super("/gerencial");
    }

    parseFiltro(params: any) {

        if (params && ('query' in params)) {
            return JSON.parse(params['query'])
        } else {
            return false
        }
    }

    getFiltro(query: any) {

        if (!query) {
            return false;
        }
        
        const match: any = {};
        if (('projeto' in query) && ('tipoProjeto' in query['projeto'])) {
            match['tipoProjetoNome'] = {$in: query['projeto']['tipoProjeto']}
        }

        if (('cliente' in query) && ('nomeFantasia' in query['cliente'])) {
            match['clienteNomeFantasia'] = {$in: query['cliente']['nomeFantasia']}
        }

        // if ('dataInicial' in query) {
        //     match['dataInicial'] = {$in: query['dataInicial']}
        // }

        // if ('dataFinal' in query) {
        //     match['dataInicial'] = {$eq: ['', query['dataInicial']]}
        // }


        if (Object.keys(match).length > 0) {
            const matand: any[] = [];   
            return match
        }

        return false;
    }

    @Routing.Find("/status")
    public async status() {
        return "ok";
    }

    @Routing.Find("/tiposProjeto")
    public async getProdutos(params?: any): Promise<TipoProjeto[]> {
        const model = new TipoProjetoModel();
        const tiposProjeto = await model.find();
        return tiposProjeto;
    }

    @Routing.Find("/clientes")
    public async getClientes(params?: any): Promise<Clientes[]> {
        const model = new ClientesModel();
        const clientes = await model.find();
        return clientes;
    }

    @Routing.Find("/relatorios")
    public async getRelatorios(params?: any): Promise<Clientes[]> {
        const model = new ClientesModel();
        const clientes = await model.find();
        return clientes;
    }

    @Routing.Find("/receitaPorCliente")
    public async getReceitaPorCliente(params?: any): Promise<IhcPrincipal[]> {
        const model = new IhcPrincipalModel();
        const pipeline = [];

        let query = this.parseFiltro(params.query);

        const match = this.getFiltro(query);
        if (match) {
            pipeline.push({$match: match})
        }

        pipeline.push(
            { $unwind: '$alocacao' }
        );

        const dataAnd = [];
        if (!query) query = {};

        if ('dataInicial' in query) {
            dataAnd.push({'alocacao.dataReferencia': {$gte: query['dataInicial']}});
        }

        if ('dataFinal' in query) {
            dataAnd.push({'alocacao.dataReferencia': {$lte: query['dataFinal']}});
        }
        if (dataAnd.length > 0) {
            pipeline.push({
                $match: {
                    $and: dataAnd
                }
            });
        }

        pipeline.push(
            {
                $group: {
                    '_id': {
                        'clienteId': '$clienteId',
                        'clienteNome': '$clienteNome'
                    },
                    'totalHoras': { $sum: '$alocacao.qtdHorasApontadas' },
                    'valorTotal': { $sum: { $multiply: ['$alocacao.qtdHorasApontadas', '$alocacao.valorHora'] } }
                }
            },
            {
                $project: {
                    '_id': 0,
                    'clienteId': '$_id.clienteId',
                    'clienteNome': '$_id.clienteNome',
                    'totalHoras': '$totalHoras',
                    'valorTotal': '$valorTotal'
                }
            }
        )
        const data = await model.aggregate(pipeline);
        return data;
    }

    @Routing.Find("/receitaPorProjeto")
    public async getReceitaPorProjeto(params?: any): Promise<IhcPrincipal[]> {
        const model = new IhcPrincipalModel();
        const pipeline = [];
        let query = this.parseFiltro(params.query);

        const match = this.getFiltro(query);
        if (match) {
            pipeline.push({$match: match})
        }

        pipeline.push(
            { $unwind: '$alocacao' }
        );

        const dataAnd = [];
        if (!query) query = {};

        if ('dataInicial' in query) {
            dataAnd.push({'alocacao.dataReferencia': {$gte: query['dataInicial']}});
        }

        if ('dataFinal' in query) {
            dataAnd.push({'alocacao.dataReferencia': {$lte: query['dataFinal']}});
        }
        if (dataAnd.length > 0) {
            pipeline.push({
                $match: {
                    $and: dataAnd
                }
            });
        }

        pipeline.push(
            {
                $group: {
                    '_id': {
                        'projetoId': '$projetoId',
                        'projetoNome': '$projetoNome'
                    },
                    'totalHoras': { $sum: '$alocacao.qtdHorasApontadas' },
                    'valorTotal': { $sum: { $multiply: ['$alocacao.qtdHorasApontadas', '$alocacao.valorHora'] } }
                }
            },
            {
                $project: {
                    '_id': 0,
                    'projetoId': '$_id.projetoId',
                    'projetoNome': '$_id.projetoNome',
                    'totalHoras': '$totalHoras',
                    'valorTotal': '$valorTotal'
                }
            }
        )
        const data = await model.aggregate(pipeline);
        return data;
    }
    
    @Routing.Find("/receitaPorAno")
    public async getReceitaPorAno(params?: any): Promise<IhcPrincipal[]> {
        const model = new IhcPrincipalModel();
        const pipeline = [];
        let query = this.parseFiltro(params.query);

        const match = this.getFiltro(query);
        if (match) {
            pipeline.push({$match: match})
        }

        pipeline.push(
            { $unwind: '$alocacao' }
        );

        const dataAnd = [];
        if (!query) query = {};

        if ('dataInicial' in query) {
            dataAnd.push({'alocacao.dataReferencia': {$gte: query['dataInicial']}});
        }

        if ('dataFinal' in query) {
            dataAnd.push({'alocacao.dataReferencia': {$lte: query['dataFinal']}});
        }
        if (dataAnd.length > 0) {
            pipeline.push({
                $match: {
                    $and: dataAnd
                }
            });
        }

        pipeline.push(
            {$addFields: {
                'ano': { $substr: [ '$alocacao.dataReferencia', 0, 4 ] }
            }},
            {$group: {
                '_id': '$ano',
                'totalHoras': {$sum: '$alocacao.qtdHorasApontadas'},
                'valorTotal': {$sum: {$multiply: ['$alocacao.qtdHorasApontadas', '$alocacao.valorHora']}}
            }},
            {$sort: {'_id': 1}}
        )
        const data = await model.aggregate(pipeline);
        return data;
    }
    

    @Routing.Find("/diferencaHoras")
    public async getDiferencaHoras(params?: any): Promise<IhcPrincipal[]> {
        const model = new IhcPrincipalModel();
        const pipeline = [];
        const query = this.parseFiltro(params.query);

        const match = this.getFiltro(query);
        if (match) {
            pipeline.push({$match: match})
        }

        pipeline.push(
            { $unwind: '$alocacao' }
        );

        const dataAnd = [];

        if ('dataInicial' in query) {
            dataAnd.push({'dataProjeto': {$gte: query['dataInicial']}});
        }

        if ('dataFinal' in query) {
            dataAnd.push({'dataProjeto': {$lte: query['dataFinal']}});
        }
        if (dataAnd.length > 0) { 
            pipeline.push({$addFields: { 'dataProjeto': {$dateFromString: {
                dateString: '$dataInicio',
              format: '%d/%m/%Y'
           }}}},
           {$match: {
               $and: dataAnd
           }});
        }

        pipeline.push(
            {$facet: {
                'planejamento': [
                    {$unwind: '$planejamento'},
                    {$group: {
                        '_id': {
                            'projetoId': '$projetoId',
                            'projetoNome': '$projetoNome'
                        },
                        'totalHoras': {$sum: '$planejamento.qtdHoras'}
                    }},
                    {$project: {
                        '_id': 0,
                        'projetoId': '$_id.projetoId',
                        'projetoNome': '$_id.projetoNome',
                        'totalHoras': '$totalHoras'
                    }},
                    {$sort: {'projetoNome': 1}}
                ],
                'alocacao': [
                    {$unwind: '$alocacao'},
                    {$group: {
                        '_id': {
                            'projetoId': '$projetoId',
                            'projetoNome': '$projetoNome'
                        },
                        'totalHoras': {$sum: '$alocacao.qtdHorasAlocadas'}
                    }},
                    {$project: {
                        '_id': 0,
                        'projetoId': '$_id.projetoId',
                        'projetoNome': '$_id.projetoNome',
                        'totalHoras': '$totalHoras'
                    }},
                    {$sort: {'projetoNome': 1}}
                ],
                'apontamento': [
                    {$unwind: '$alocacao'},
                    {$group: {
                        '_id': {
                            'projetoId': '$projetoId',
                            'projetoNome': '$projetoNome'
                        },
                        'totalHoras': {$sum: '$alocacao.qtdHorasApontadas'}
                    }},
                    {$project: {
                        '_id': 0,
                        'projetoId': '$_id.projetoId',
                        'projetoNome': '$_id.projetoNome',
                        'totalHoras': '$totalHoras'
                    }},
                    {$sort: {'projetoNome': 1}}
                ]
            }}
        )
        const data = await model.aggregate(pipeline);
        return data;
    }

}
