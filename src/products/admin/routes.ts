import { Routing, ServiceHookContext, Method } from "../../core/routing";
import { CRUDMethods } from "../../core/crud";


import { AuthRoutes } from "../../auth/routes";
import { UserAccessType } from "../../auth/model";

import { NullableId } from "@feathersjs/feathers";
import { RequestParams } from "nodemailer/lib/xoauth2";
import { BadRequest } from "@feathersjs/errors";
import { PaymentRoutes } from "../../payment/routes";
import { TipoProjetoModel } from "../model";


const adminPrefix = '/api-admin/crud';


@CRUDMethods('/tipoProjeto', TipoProjetoModel)
export class ProductsAdminRoutes extends Routing {
    constructor() {
        super("/produtos" + adminPrefix);
    }

    protected before = AuthRoutes.getAuthHook(UserAccessType.Admin);


}
