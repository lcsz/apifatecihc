import { MongoDbModel } from "../core/mongodb-model";
import { Model } from "../core/persisted-model";

export interface TipoProjeto extends Model {
    _id : string; 
    nome : string;
}
export class TipoProjetoModel extends MongoDbModel<TipoProjeto> {
    constructor() {
        super("ihc_tipoProjeto");
    }
}


export interface Clientes extends Model {
    _id : string; 
    nome : string;
}
export class ClientesModel extends MongoDbModel<Clientes> {
    constructor() {
        super("ihc_cliente");
    }
}

export interface IhcPrincipal extends Model {
    _id : string; 
    nome : string;
}
export class IhcPrincipalModel extends MongoDbModel<IhcPrincipal> {
    constructor() {
        super("ihc_principal");
    }
}